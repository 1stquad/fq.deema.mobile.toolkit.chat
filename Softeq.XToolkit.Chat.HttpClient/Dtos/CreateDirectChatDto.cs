﻿namespace Softeq.XToolkit.Chat.HttpClient.Dtos
{
    internal class CreateDirectChatDto
    {
        public string MemberId { get; set; }
    }
}
