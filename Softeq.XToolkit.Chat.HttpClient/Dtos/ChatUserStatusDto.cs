﻿namespace Softeq.XToolkit.Chat.HttpClient.Dtos
{
    internal enum ChatUserStatusDto
    {
        Online,
        Offline
    }
}