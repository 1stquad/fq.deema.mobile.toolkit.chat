﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.HttpClient.Dtos
{
    internal enum MessageTypeDto
    {
        Default = 0,
        Notification = 1
    }
}
