﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.HttpClient.Dtos
{
    internal enum ChatUserRoleDto
    {
        User = 0,
        Admin = 1
    }
}
