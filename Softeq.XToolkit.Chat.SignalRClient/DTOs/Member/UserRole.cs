﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.SignalRClient.DTOs.Member
{
    internal enum UserRole
    {
        User = 0,
        Admin = 1
    }
}
