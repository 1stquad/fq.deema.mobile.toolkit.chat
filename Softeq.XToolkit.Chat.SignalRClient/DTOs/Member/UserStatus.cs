﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.SignalRClient.DTOs.Member
{
    internal enum UserStatus
    {
        Online,
        Offline
    }
}
