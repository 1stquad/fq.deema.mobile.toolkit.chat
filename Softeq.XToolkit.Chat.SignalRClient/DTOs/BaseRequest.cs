﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.SignalRClient.DTOs
{
    internal class BaseRequest
    {
        public string RequestId { get; set; }
    }
}
