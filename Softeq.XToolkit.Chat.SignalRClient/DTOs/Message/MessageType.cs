﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.SignalRClient.DTOs.Message
{
    internal enum MessageType
    {
        Default,
        Notification
    }
}
