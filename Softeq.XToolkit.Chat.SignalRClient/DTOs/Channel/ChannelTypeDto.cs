﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.SignalRClient.DTOs.Channel
{
    internal enum ChannelTypeDto
    {
        Public = 0,
        Private = 1,
        Direct = 2
    }
}
