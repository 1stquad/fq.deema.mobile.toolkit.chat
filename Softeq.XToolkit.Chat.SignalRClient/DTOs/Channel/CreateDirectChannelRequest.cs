﻿namespace Softeq.XToolkit.Chat.SignalRClient.DTOs.Channel
{
    internal class CreateDirectChannelRequest : BaseRequest
    {
        public string MemberId { get; set; }
    }
}
