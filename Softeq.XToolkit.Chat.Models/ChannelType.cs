﻿namespace Softeq.XToolkit.Chat.Models
{
    public enum ChannelType
    {
        Public = 0,
        Private = 1,
        Direct = 2
    }
}
