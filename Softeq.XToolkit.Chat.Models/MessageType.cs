﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Chat.Models
{
    public enum MessageType
    {
        Default = 0,
        Info = 1
    }
}
