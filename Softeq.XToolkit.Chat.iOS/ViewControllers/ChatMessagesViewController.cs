﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

using System;
using System.Linq;
using System.Threading.Tasks;
using AsyncDisplayKitBindings;
using CoreGraphics;
using Foundation;
using Softeq.XToolkit.Bindings;
using Softeq.XToolkit.Chat.iOS.Controls;
using Softeq.XToolkit.Chat.iOS.TableSources;
using Softeq.XToolkit.Chat.iOS.Views;
using Softeq.XToolkit.Chat.ViewModels;
using Softeq.XToolkit.Common;
using Softeq.XToolkit.Common.Command;
using Softeq.XToolkit.Common.Extensions;
using Softeq.XToolkit.WhiteLabel.iOS;
using Softeq.XToolkit.WhiteLabel.iOS.Extensions;
using Softeq.XToolkit.WhiteLabel.Threading;
using UIKit;

namespace Softeq.XToolkit.Chat.iOS.ViewControllers
{
    //TODO VPY: hard refactoring needed
    public partial class ChatMessagesViewController : ViewControllerBase<ChatMessagesViewModel>
    {
        private const int DefaultScrollDownBottomConstraintValue = 8;
        private const int MinCellHeight = 50;
        private const string ContentSizeKey = "contentSize";

        protected readonly ASTableNode TableNode = new ASTableNode(UITableViewStyle.Grouped);

        private WeakReferenceEx<GroupedTableDataSource<DateTimeOffset, ChatMessageViewModel>> _dataSourceRef;
        private bool _isAutoScrollAvailable;
        private bool _isAutoScrollToBottomEnabled = true;
        private bool _shouldUpdateTableViewContentOffset;
        private NSLayoutConstraint _tableViewBottomConstraint;
        private ConnectionStatusView _customTitleView;
        private MessagesTableDelegate _tableDelegate;
        private ContextMenuHandler<ChatMessageViewModel> _contextMenuHandler;

        public ChatMessagesViewController(IntPtr handle) : base(handle) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            SetupInputAccessoryView();

            _customTitleView = new ConnectionStatusView(CGRect.Empty);

            CustomNavigationItem.AddTitleView(_customTitleView);
            CustomNavigationItem.SetCommand(
                UIImage.FromBundle(StyleHelper.Style.BackButtonBundleName),
                ViewModel.BackCommand,
                true);

            if (ViewModel.HasInfo)
            {
                CustomNavigationItem.SetCommand(
                    UIImage.FromBundle(StyleHelper.Style.ChatDetailsButtonBundleName),
                    ViewModel.ShowInfoCommand,
                    false);
            }

            InitTableView();

            _contextMenuHandler = new ContextMenuHandler<ChatMessageViewModel>(CreateContextMenuComponentForViewModel);

            InputBar.ViewDidLoad(this);

            InputBar.SetCommandWithArgs(nameof(InputBar.SendRaised), ViewModel.MessageInput.SendMessageCommand);
            InputBar.SetCommand(nameof(InputBar.PickerWillOpen), new RelayCommand(UnregisterKeyboardObservers));
            InputBar.EditingClose.SetCommand(ViewModel.MessageInput.CancelEditingCommand);
            InputBar.EditingClose.SetImage(UIImage.FromBundle(StyleHelper.Style.CloseButtonImageBoundleName), UIControlState.Normal);
            InputBar.SetLabels(
                ViewModel.MessageInput.EditMessageHeaderString,
                ViewModel.MessageInput.EnterMessagePlaceholderString);

            ScrollToBottomButton.SetCommand(new RelayCommand(() => ScrollToBottom(true)));
            ScrollToBottomButton.SetBackgroundImage(UIImage.FromBundle(StyleHelper.Style.ScrollDownBoundleName), UIControlState.Normal);
        }

        public override void ViewWillAppear(bool animated)
        {
            RegisterKeyboardObservers();

            base.ViewWillAppear(animated);

            ViewModel.MessageAddedCommand = new RelayCommand(OnMessageAdded);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (SafeAreaBottomOffset > 0)
            {
                InputBar.InvalidateIntrinsicContentSize();
            }

            _tableDelegate.ScrollPositionChanged += TableViewDelegateScrollPositionChanged;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            if (_isKeyboardOpened)
            {
                HideKeyboard();
            }

            ViewModel.MessageAddedCommand = null;

            // TODO YP: need check this approach
            if (NavigationController != null && !NavigationController.ViewControllers.Contains(this))
            {
                _dataSourceRef.Target?.UnsubscribeItemsChanged();
            }

            UnregisterKeyboardObservers();

            _tableDelegate.ScrollPositionChanged -= TableViewDelegateScrollPositionChanged;
        }

        public override void ObserveValue(NSString keyPath, NSObject ofObject, NSDictionary change, IntPtr context)
        {
            if (keyPath != ContentSizeKey)
            {
                base.ObserveValue(keyPath, ofObject, change, context);
                return;
            }

            var oldSize = ((NSValue)new NSObservedChange(change).OldValue).CGSizeValue;
            var newSize = ((NSValue)new NSObservedChange(change).NewValue).CGSizeValue;

            if (ofObject is ASTableView)
            {
                TableViewContentSizeChanged(newSize, oldSize);
                return;
            }
        }

        protected override void DoAttachBindings()
        {
            base.DoAttachBindings();

            Bindings.Add(this.SetBinding(() => ViewModel.MessageInput.MessageBody, () => InputBar.Input.Text, BindingMode.TwoWay));
            Bindings.Add(this.SetBinding(() => ViewModel.MessageInput.IsInEditMessageMode).WhenSourceChanges(() =>
            {
                if (ViewModel.MessageInput.IsInEditMessageMode)
                {
                    InputBar.StartEditing(ViewModel.MessageInput.EditedMessageOriginalBody);
                }
                InputBar.ChangeEditingMode(ViewModel.MessageInput.IsInEditMessageMode);
            }));
            Bindings.Add(this.SetBinding(() => ViewModel.MessagesList.Messages, () => _dataSourceRef.Target.DataSource));
            Bindings.Add(this.SetBinding(() => ViewModel.ConnectionStatus.ConnectionStatusText).WhenSourceChanges(() =>
            {
                _customTitleView.Update(ViewModel.ConnectionStatus);
            }));
            Bindings.Add(this.SetBinding(() => ViewModel.MessageInput.MessageBody).WhenSourceChanges(() =>
            {
                InputBar.SetTextPlaceholdervisibility(string.IsNullOrEmpty(ViewModel.MessageInput.MessageBody));
            }));
        }

        private void InitTableView()
        {
            TableNode.View.RegisterNibForHeaderFooterViewReuse(MessagesDateHeaderViewCell.Nib, MessagesDateHeaderViewCell.Key);

            TableNode.Inverted = true;
            TableNode.ShouldAnimateSizeChanges = false;
            TableNode.LayerBacked = true;
            TableNode.RasterizationScale = 1;

            InitTableViewDelegate();

            InitTableViewAsync().SafeTaskWrapper();
        }

        private void InitTableViewDelegate()
        {
            _tableDelegate = new MessagesTableDelegate(TableNode.Inverted)
            {
                SectionHeaderHeight = 35f,
                GetViewForHeaderDelegate = GetHeader
            };
            _tableDelegate.MoreDataRequested += OnMoreDataRequested;
        }

        private async Task InitTableViewAsync()
        {
            // delay is need to delay UI thread freezing while TableNode items are loaded
            // thus previous screen will not be frozen
            await Task.Delay(1);

            TableNode.View.KeyboardDismissMode = UIScrollViewKeyboardDismissMode.Interactive;
            TableNode.View.AddGestureRecognizer(new UITapGestureRecognizer(HideKeyboard));
            TableNode.View.TranslatesAutoresizingMaskIntoConstraints = false;
            TableNode.View.BackgroundColor = UIColor.FromRGB(245, 245, 245);
            MainView.InsertSubview(TableNode.View, 1);

            _tableViewBottomConstraint = TableNode.View.BottomAnchor.ConstraintEqualTo(MainView.BottomAnchor, -InputBarHeight);

            NSLayoutConstraint.ActivateConstraints(new[]
            {
                TableNode.View.RightAnchor.ConstraintEqualTo(MainView.RightAnchor),
                TableNode.View.LeftAnchor.ConstraintEqualTo(MainView.LeftAnchor),
                TableNode.View.TopAnchor.ConstraintEqualTo(MainView.SafeAreaLayoutGuide.TopAnchor),
                _tableViewBottomConstraint
            });

            var tableSource = new GroupedTableDataSource<DateTimeOffset, ChatMessageViewModel>(
                ViewModel.MessagesList.Messages,
                TableNode.View,
                viewModel => new ChatMessageNode(viewModel, _contextMenuHandler),
                TableNode.Inverted);

            TableNode.View.EstimatedRowHeight = MinCellHeight;
            TableNode.View.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            _dataSourceRef = WeakReferenceEx.Create(tableSource);
            TableNode.DataSource = tableSource;
            TableNode.Delegate = _tableDelegate;
            TableNode.View.AddObserver(this, ContentSizeKey, NSKeyValueObservingOptions.OldNew, IntPtr.Zero);
            TableNode.SetTuningParameters(new ASRangeTuningParameters { leadingBufferScreenfuls = 1, trailingBufferScreenfuls = 1 }, ASLayoutRangeType.Display);
            TableNode.SetTuningParameters(new ASRangeTuningParameters { leadingBufferScreenfuls = 1, trailingBufferScreenfuls = 1 }, ASLayoutRangeType.Preload);
            TableNode.ReloadData();
            TableNode.LeadingScreensForBatching = 3;
        }

        private UIView GetHeader(UITableView tableView, nint section)
        {
            var sectionFooter = (MessagesDateHeaderViewCell)tableView.DequeueReusableHeaderFooterView(MessagesDateHeaderViewCell.Key);
            if (section < ViewModel.MessagesList.Messages.Count)
            {
                var adjustedSectionIndex = TableNode.Inverted ? ViewModel.MessagesList.Messages.Count - 1 - section : section;
                var date = ViewModel.MessagesList.Messages[(int)adjustedSectionIndex].Key;
                sectionFooter.Title = ViewModel.GetDateString(date);
            }
            return sectionFooter;
        }

        private async void OnMoreDataRequested(ASBatchContext obj)
        {
            await ViewModel.MessagesList.LoadOlderMessagesAsync();
            obj.CompleteBatchFetching(true);
        }

        private void TableViewContentSizeChanged(CGSize newSize, CGSize oldSize)
        {
            var deltaHeight = newSize.Height - oldSize.Height;

            if (!_isAutoScrollToBottomEnabled && _shouldUpdateTableViewContentOffset && deltaHeight > MinCellHeight)
            {
                _shouldUpdateTableViewContentOffset = false;

                TableNode.SetContentOffset(new CGPoint(0, TableNode.ContentOffset.Y + deltaHeight), false);
            }

            //Console.WriteLine($"XXX: {newSize.Height} - {oldSize.Height} = {deltaHeight}, isAutoScroll:{_isAutoScrollToBottomEnabled}");
        }

        private void TableViewDelegateScrollPositionChanged(object sender, nfloat scrollPosition)
        {
            TableViewScrollChanged(scrollPosition);
        }

        private void TableViewScrollChanged(nfloat scrollViewOffsetY)
        {
            var isAutoScrollAvailable = scrollViewOffsetY < TableNode.View.Frame.Height;
            if (isAutoScrollAvailable != _isAutoScrollAvailable)
            {
                _isAutoScrollAvailable = isAutoScrollAvailable;

                UIView.Transition(ScrollToBottomButton, 0.4,
                              UIViewAnimationOptions.TransitionCrossDissolve,
                              () => ScrollToBottomButton.Hidden = isAutoScrollAvailable,
                              () => { });

                _isAutoScrollToBottomEnabled = isAutoScrollAvailable;
            }

            UpdateScrollDownButtonPosition();
        }

        private void OnMessageAdded()
        {
            _shouldUpdateTableViewContentOffset = true;

            if (_isAutoScrollToBottomEnabled)
            {
                ScrollToBottom();
            }
        }

        private void ScrollToBottom(bool isAnimated = false)
        {
            Execute.BeginOnUIThread(() =>
            {
                var index = NSIndexPath.FromRowSection(0, 0);
                TableNode.ScrollToRowAtIndexPath(index, UITableViewScrollPosition.None, isAnimated);
            });
        }

        private void UpdateScrollDownButtonPosition()
        {
            if (InputBar.Superview == null || View.Superview == null)
            {
                return;
            }

            var newScrollDownBottomOffset = View.Superview.Frame.Height - InputBar.Superview.Frame.Y;
            if (newScrollDownBottomOffset < InputBarHeight)
            {
                newScrollDownBottomOffset += SafeAreaBottomOffset;
            }
            ScrollDownBottomConstraint.Constant = newScrollDownBottomOffset + DefaultScrollDownBottomConstraintValue;
        }

        private void UpdateScrollDownButtonPositionWithAnimation()
        {
            UIView.Animate(0.1, () =>
            {
                UpdateScrollDownButtonPosition();
                MainView.LayoutIfNeeded();
            });
        }

        private ContextMenuComponent CreateContextMenuComponentForViewModel(ChatMessageViewModel message)
        {
            var contextMenuComponent = new ContextMenuComponent();
            var commandActions = ViewModel.GetCommandActionsForMessage(message);
            switch (commandActions.Count)
            {
                case 1:
                    contextMenuComponent.AddCommand(ContextMenuActions.Delete, commandActions[0]);
                    break;
                case 2:
                    contextMenuComponent.AddCommand(ContextMenuActions.Edit, commandActions[0]);
                    contextMenuComponent.AddCommand(ContextMenuActions.Delete, commandActions[1]);
                    break;
            }
            return contextMenuComponent;
        }
        
        [Export(ContextMenuActions.Edit)]
        private void Edit() => _contextMenuHandler.ExecuteCommand(ContextMenuActions.Edit);

        [Export(ContextMenuActions.Delete)]
        private void Delete() => _contextMenuHandler.ExecuteCommand(ContextMenuActions.Delete);

        private class MessagesTableDelegate : GroupedTableDelegate
        {
            private GroupedTableDataSource<DateTimeOffset, ChatMessageViewModel> _source;

            public event EventHandler LastItemRequested;
            public event EventHandler<nfloat> ScrollPositionChanged;

            public MessagesTableDelegate(bool isInverted) : base(isInverted)
            {
            }

            public override void WillDisplayNodeForRowAtIndexPath(ASTableView tableView, NSIndexPath indexPath)
            {
                if (_source == null)
                {
                    var table = tableView as ASTableView;
                    _source = table.TableNode.DataSource as GroupedTableDataSource<DateTimeOffset, ChatMessageViewModel>;
                }

                var count = _source.DataSource[0].Count;
                if (indexPath.Section == _source.DataSource.Count - 1 && indexPath.Row == count - 1)
                {
                    LastItemRequested?.Invoke(this, EventArgs.Empty);
                }
            }

            [Export("scrollViewDidScroll:")]
            public void Scrolled(UIScrollView scrollView)
            {
                ScrollPositionChanged?.Invoke(this, scrollView.ContentOffset.Y);
            }
        }
    }
}
